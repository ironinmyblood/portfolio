import React, { Component } from 'react';
import PropTypes from 'prop-types';
import BezierEasing from 'bezier-easing';
import ScrollManager from 'window-scroll-manager';

const WINDOW_EXISTS = typeof window !== 'undefined';

const START_END_DURATION_REGEX = /^-?\d+(\.\d+)?(px|vh|%)?$/;

const DEFAULT_UNIT = 'px';
const DEFAULT_ANGLE_UNIT = 'deg';
const ANGLE_PROPERTIES = [
  'rotate',
  'rotateX',
  'rotateY',
  'rotateZ',
  'skew',
  'skewX',
  'skewY',
  'skewZ',
  'hueRotate',
];

const EASINGS = {
  ease: [0.25, 0.1, 0.25, 1.0],
  easeIn: [0.42, 0.0, 1.00, 1.0],
  easeOut: [0.00, 0.0, 0.58, 1.0],
  easeInOut: [0.42, 0.0, 0.58, 1.0],
  easeInSine: [0.47, 0, 0.745, 0.715],
  easeOutSine: [0.39, 0.575, 0.565, 1],
  easeInOutSine: [0.445, 0.05, 0.55, 0.95],
  easeInQuad: [0.55, 0.085, 0.68, 0.53],
  easeOutQuad: [0.25, 0.46, 0.45, 0.94],
  easeInOutQuad: [0.455, 0.03, 0.515, 0.955],
  easeInCubic: [0.55, 0.055, 0.675, 0.19],
  easeOutCubic: [0.215, 0.61, 0.355, 1],
  easeInOutCubic: [0.645, 0.045, 0.355, 1],
  easeInQuart: [0.895, 0.03, 0.685, 0.22],
  easeOutQuart: [0.165, 0.84, 0.44, 1],
  easeInOutQuart: [0.77, 0, 0.175, 1],
  easeInQuint: [0.755, 0.05, 0.855, 0.06],
  easeOutQuint: [0.23, 1, 0.32, 1],
  easeInOutQuint: [0.86, 0, 0.07, 1],
  easeInExpo: [0.95, 0.05, 0.795, 0.035],
  easeOutExpo: [0.19, 1, 0.22, 1],
  easeInOutExpo: [1, 0, 0, 1],
  easeInCirc: [0.6, 0.04, 0.98, 0.335],
  easeOutCirc: [0.075, 0.82, 0.165, 1],
  easeInOutCirc: [0.785, 0.135, 0.15, 0.86],
};

const REGEX_0_255 = '(1?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])';

const REGEX_0_1 = '([01](\\.\\d+)?)';

const REGEX_TWO_HEX_DIGITS = '([a-f\\d]{2})';

const HEX_REGEX = new RegExp(`^#${REGEX_TWO_HEX_DIGITS}${REGEX_TWO_HEX_DIGITS}${REGEX_TWO_HEX_DIGITS}$`, 'i');
const RGB_REGEX = new RegExp(`^rgb\\(${REGEX_0_255},${REGEX_0_255},${REGEX_0_255}\\)$`, 'i');
const RGBA_REGEX = new RegExp(`^rgba\\(${REGEX_0_255},${REGEX_0_255},${REGEX_0_255},${REGEX_0_1}\\)$`, 'i');

const SCROLL_OFFSET = 50;

const RESIZE_DEBOUNCE_TIMEOUT = 150;

const TRANSFORM_MAP = {
  rotate: (value, unit: DEFAULT_ANGLE_UNIT) => `rotate(${value}${unit})`,
  rotateX: (value, unit: DEFAULT_ANGLE_UNIT) => `rotateX(${value}${unit})`,
  rotateY: (value, unit: DEFAULT_ANGLE_UNIT) => `rotateY(${value}${unit})`,
  rotateZ: (value, unit: DEFAULT_ANGLE_UNIT) => `rotateZ(${value}${unit})`,
  scale: value => `scale(${value})`,
  scaleX: value => `scaleX(${value})`,
  scaleY: value => `scaleY(${value})`,
  scaleZ: value => `scaleZ(${value})`,
  skew: (value, unit: DEFAULT_UNIT) => `skew(${value}${unit})`,
  skewX: (value, unit: DEFAULT_UNIT) => `skewX(${value}${unit})`,
  skewY: (value, unit: DEFAULT_UNIT) => `skewY(${value}${unit})`,
  skewZ: (value, unit: DEFAULT_UNIT) => `skewZ(${value}${unit})`,
  translateX: (value, unit: DEFAULT_UNIT) => `translateX(${value}${unit})`,
  translateY: (value, unit: DEFAULT_UNIT) => `translateY(${value}${unit})`,
  translateZ: (value, unit: DEFAULT_UNIT) => `translateZ(${value}${unit})`,
};

const ORDER_OF_TRANSFORMS = [
  'translateX',
  'translateY',
  'translateZ',
  'skew',
  'skewX',
  'skewY',
  'skewZ',
  'rotate',
  'rotateX',
  'rotateY',
  'rotateZ',
  'scale',
  'scaleX',
  'scaleY',
  'scaleZ',
];

const COLOR_PROPERTIES = [
  'backgroundColor',
  'borderBottomColor',
  'borderColor',
  'borderLeftColor',
  'borderRightColor',
  'borderTopColor',
  'color',
  'fill',
  'strokeColor',
];

const FILTER_MAP = {
  blur: (value, unit: DEFAULT_UNIT) => `blur(${value}${unit})`,
  brightness: value => `brightness(${value})`,
  contrast: value => `contrast(${value})`,
  grayscale: value => `grayscale(${value})`,
  hueRotate: (value, unit: DEFAULT_ANGLE_UNIT) => `hue-rotate(${value}${unit})`,
  invert: value => `invert(${value})`,
  opacityFilter: value => `opacity(${value})`,
  saturate: value => `saturate(${value})`,
  sepia: value => `sepia(${value})`,
};

const FILTER_PROPERTIES = [
  'blur',
  'brightness',
  'contrast',
  'grayscale',
  'hueRotate',
  'invert',
  'opacityFilter',
  'saturate',
  'sepia',
];

const PROPS_TO_OMIT = [
  'animateWhenNotInViewport',
  'children',
  'className',
  'freeze',
  'parallaxData',
  'style',
  'tagName',
];

function getElementTop(el) {
  let top = 0;
  let element = el;

  do {
    top += element.offsetTop || 0;
    element = element.offsetParent;
  } while (element);

  return top;
}

function getUnit(property, unit) {
  let propertyUnit = unit || DEFAULT_UNIT;

  if (ANGLE_PROPERTIES.indexOf(property) >= 0) {
    propertyUnit = unit || DEFAULT_ANGLE_UNIT;
  }

  return propertyUnit;
}

function getValueInPx(value, maxScroll) {
  const floatValue = parseFloat(value);
  const unit = value.match(START_END_DURATION_REGEX)[2] || null;
  const vh = window.innerHeight / 100;
  let valueInPx = value;

  switch (unit) {
    case 'vh':
      valueInPx = vh * floatValue;
      break;
    case '%':
      valueInPx = maxScroll * floatValue / 100;
      break;
    default:
      valueInPx = floatValue;
  }

  return valueInPx;
}

function convertPropToPixels(propName, propValue, maxScroll, offset = 0) {
  let propValueInPx = propValue;
  const isElement = propValue instanceof HTMLElement;
  const keyCodes = {
    ZERO: 48,
    NINE: 57,
  };

  if (typeof propValue === 'number') {
    propValueInPx = propValue;
  } else if (START_END_DURATION_REGEX.test(propValue)) {
    propValueInPx = getValueInPx(propValue, maxScroll);
  } else if (
    isElement ||
    typeof propValue === 'string' &&
    (propValue.charCodeAt(0) < keyCodes.ZERO || propValue.charCodeAt(0) > keyCodes.NINE)
  ) {
    const element = isElement ? propValue : document.querySelector(propValue);

    if (!element) {
      console.warn(`Parallax, ERROR: ${propName} selector matches no elements: "${propValue}"`);
      return null;
    }

    if (propName === 'start' || propName === 'end') {

      propValueInPx = getElementTop(element) - window.innerHeight;
    } else if (propName === 'duration') {

      propValueInPx = element.offsetHeight;
    }
  } else {
    console.warn(`Parallax, ERROR: "${propValue}" is not a valid ${propName} value, check documenation`);
    return null;
  }

  let offsetInPx = 0;

  if (typeof offset === 'number') {
    offsetInPx = offset;
  } else if (START_END_DURATION_REGEX.test(offset)) {
    offsetInPx = getValueInPx(offset, maxScroll);
  }

  propValueInPx += offsetInPx;

  if (propValueInPx < 0) {
    propValueInPx = 0;
  }

  return propValueInPx;
}


function hexToObject(hex) {
  const color = hex.length === 4 ? `#${hex[1]}${hex[1]}${hex[2]}${hex[2]}${hex[3]}${hex[3]}` : hex;
  const result = HEX_REGEX.exec(color);

  if (!result) {
    console.warn(`Parallax, ERROR: hex color is not in the right format: "${hex}"`);
    return null;
  }

  return {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16),
    a: 1,
  };
}

function rgbToObject(rgb) {
  const isRgba = rgb.toLowerCase().indexOf('rgba') === 0;
  const color = rgb.replace(/ /g, '');
  const result = isRgba ? RGBA_REGEX.exec(color) : RGB_REGEX.exec(color);

  if (!result) {
    console.warn(`Parallax, ERROR: rgb or rgba color is not in the right format: "${rgb}"`);
    return null;
  }

  return {
    r: parseInt(result[1], 10),
    g: parseInt(result[2], 10),
    b: parseInt(result[3], 10),
    a: isRgba ? parseFloat(result[4]) : 1,
  };
}

function parallax(scrollPosition, start, duration, startValue, endValue, easing) {
  let min = startValue;
  let max = endValue;
  const invert = startValue > endValue;


  if (typeof startValue !== 'number') {
    console.warn(`Parallax, ERROR: startValue is not a number (type: "${typeof endValue}", value: "${endValue}")`);
    return null;
  }

  if (typeof endValue !== 'number') {
    console.warn(`Parallax, ERROR: endValue is not a number (type: "${typeof endValue}", value: "${endValue}")`);
    return null;
  }

  if (typeof duration !== 'number' || duration === 0) {
    console.warn(`Parallax, ERROR: duration is zero or not a number (type: "${typeof duration}", value: "${duration}")`);
    return null;
  }

  if (invert) {
    min = endValue;
    max = startValue;
  }

  let percentage = ((scrollPosition - start) / duration);

  if (percentage > 1) {
    percentage = 1;
  } else if (percentage < 0) {
    percentage = 0;
  }

  if (easing) {
    const easingPropType = typeof easing;
    if (easingPropType === 'object' && easing.length === 4) {
      percentage = BezierEasing(
        easing[0],
        easing[1],
        easing[2],
        easing[3]
      )(percentage);
    } else if (easingPropType === 'string' && EASINGS[easing]) {
      percentage = BezierEasing(
        EASINGS[easing][0],
        EASINGS[easing][1],
        EASINGS[easing][2],
        EASINGS[easing][3]
      )(percentage);
    } else if (easingPropType === 'function') {
      percentage = easing(percentage);
    }
  }

  let value = percentage * (max - min);

  if (invert) {
    value = max - value;
  } else {
    value += min;
  }

  return parseFloat(value.toFixed(3));
}

function colorParallax(scrollPosition, start, duration, startValue, endValue, easing) {
  let startObject = null;
  let endObject = null;

  if (startValue[0].toLowerCase() === 'r') {
    startObject = rgbToObject(startValue);
  } else {
    startObject = hexToObject(startValue);
  }

  if (endValue[0].toLowerCase() === 'r') {
    endObject = rgbToObject(endValue);
  } else {
    endObject = hexToObject(endValue);
  }

  if (startObject && endObject) {
    const r = parallax(scrollPosition, start, duration, startObject.r, endObject.r, easing);
    const g = parallax(scrollPosition, start, duration, startObject.g, endObject.g, easing);
    const b = parallax(scrollPosition, start, duration, startObject.b, endObject.b, easing);
    const a = parallax(scrollPosition, start, duration, startObject.a, endObject.a, easing);

    return `rgba(${parseInt(r, 10)}, ${parseInt(g, 10)}, ${parseInt(b, 10)}, ${a})`;
  }

  return null;
}

function applyProperty(scrollPosition, propertyData, startPosition, duration, style, easing) {
  const {
    startValue,
    endValue,
    property,
    unit,
  } = propertyData;

  const isColor = COLOR_PROPERTIES.indexOf(property) > -1;
  const parallaxMethod = isColor ? colorParallax : parallax;

  const value = parallaxMethod(
    scrollPosition,
    startPosition,
    duration,
    startValue,
    endValue,
    easing
  );

  
  const transformMethod = TRANSFORM_MAP[property];
  const filterMethod = FILTER_MAP[property];
  const newStyle = style;

  if (transformMethod) {
    
    const propertyUnit = getUnit(property, unit);
    
    newStyle.transform[property] = transformMethod(value, propertyUnit);
  } else if (filterMethod) {
    
    const propertyUnit = getUnit(property, unit);
    
    newStyle.filter[property] = filterMethod(value, propertyUnit);
  } else {
    
    newStyle[property] = value;

    if (unit) {
      newStyle[property] += unit;
    }
  }

  return newStyle;
}

function getClasses(lastSegmentScrolledBy, isInSegment, parallaxData) {
  let cssClasses = null;

  if (lastSegmentScrolledBy === null) {
    cssClasses = 'Parallax--above';
  } else if (lastSegmentScrolledBy === parallaxData.length - 1 && !isInSegment) {
    cssClasses = 'Parallax--bellow';
  } else if (lastSegmentScrolledBy !== null && isInSegment) {
    const segmentName = parallaxData[lastSegmentScrolledBy].name || lastSegmentScrolledBy;

    cssClasses = `Parallax--active Parallax--in Parallax--in-${segmentName}`;
  } else if (lastSegmentScrolledBy !== null && !isInSegment) {
    const segmentName = parallaxData[lastSegmentScrolledBy].name || lastSegmentScrolledBy;
    const nextSegmentName = parallaxData[lastSegmentScrolledBy + 1].name || lastSegmentScrolledBy + 1;

    cssClasses = `Parallax--active Parallax--between Parallax--between-${segmentName}-and-${nextSegmentName}`;
  }

  return cssClasses;
}

function omit(object, keysToOmit) {

  const result = {};

  Object.keys(object).forEach(key => {
    if (keysToOmit.indexOf(key) === -1) {
      result[key] = object[key];
    }
  });

  return result;
}

function getNewState(scrollPosition, props, state, element) {
  const {
    animateWhenNotInViewport,
    disabled,
    freeze,
    parallaxData,
  } = props;
  const {
    showElement,
    parallaxStyle,
    plxStateClasses,
  } = state;

  
  if ((freeze && showElement) || !element || disabled) {
    return null;
  }

  if (!animateWhenNotInViewport) {
    const rect = element.getBoundingClientRect();
    const isTopAboveBottomEdge = rect.top < window.innerHeight + SCROLL_OFFSET;
    const isBottomBellowTopEdge = rect.top + rect.height > -SCROLL_OFFSET;

    if (!isTopAboveBottomEdge || !isBottomBellowTopEdge) {
      return null;
    }
  }

  const newState = {};
  
  let newStyle = {
    transform: {},
    filter: {},
  };

  if (!showElement) {
    newState.showElement = true;
  }

  const appliedProperties = [];
  const segments = [];
  let isInSegment = false;
  let lastSegmentScrolledBy = null;
  const maxScroll = Math.max(
    document.body.scrollHeight,
    document.body.offsetHeight,
    document.documentElement.clientHeight,
    document.documentElement.scrollHeight,
    document.documentElement.offsetHeight
  ) - window.innerHeight;

  for (let i = 0; i < parallaxData.length; i++) {
    const {
      duration,
      easing,
      endOffset,
      properties,
      startOffset,
    } = parallaxData[i];

    const start = parallaxData[i].start === 'self' ? element : parallaxData[i].start;
    const end = parallaxData[i].end === 'self' ? element : parallaxData[i].end;

    const startInPx = convertPropToPixels('start', start, maxScroll, startOffset);
    let durationInPx = null;
    let endInPx = null;

    
    if (typeof end !== 'undefined') {
      endInPx = convertPropToPixels('end', end, maxScroll, endOffset);
      durationInPx = endInPx - startInPx;
    } else {
      durationInPx = convertPropToPixels('duration', duration, maxScroll);
      endInPx = startInPx + durationInPx;
    }

    if (scrollPosition < startInPx) {
      break;
    }

    const isScrolledByStart = scrollPosition >= startInPx;

    if (isScrolledByStart) {
      lastSegmentScrolledBy = i;
    }

    if (scrollPosition >= startInPx && scrollPosition <= endInPx) {
      isInSegment = true;

      properties.forEach(propertyData => { 
        const { property } = propertyData;

        appliedProperties.push(property);

        newStyle = applyProperty(
          scrollPosition,
          propertyData,
          startInPx,
          durationInPx,
          newStyle,
          easing
        );
      });
    } else {
      segments.push({
        easing,
        durationInPx,
        properties,
        startInPx,
      });
    }
  }

  segments.forEach(data => {
    const {
      easing,
      durationInPx,
      properties,
      startInPx,
    } = data;

    properties.forEach((propertyData) => {
      const { property } = propertyData;

      if (appliedProperties.indexOf(property) > -1) {
        return;
      }

      newStyle = applyProperty(
        scrollPosition,
        propertyData,
        startInPx,
        durationInPx,
        newStyle,
        easing
      );
    });
  });

  const transformsOrdered = [];

  ORDER_OF_TRANSFORMS.forEach(transformKey => {
    if (newStyle.transform[transformKey]) {
      transformsOrdered.push(newStyle.transform[transformKey]);
    }
  });

  
  newStyle.transform = transformsOrdered.join(' ');
  newStyle.WebkitTransform = newStyle.transform;
  newStyle.MozTransform = newStyle.transform;
  newStyle.OTransform = newStyle.transform;
  newStyle.msTransform = newStyle.transform;

  const filtersArray = [];
  FILTER_PROPERTIES.forEach(filterKey => {
    if (newStyle.filter[filterKey]) {
      filtersArray.push(newStyle.filter[filterKey]);
    }
  });


  newStyle.filter = filtersArray.join(' ');
  newStyle.WebkitFilter = newStyle.filter;
  newStyle.MozFilter = newStyle.filter;
  newStyle.OFilter = newStyle.filter;
  newStyle.msFilter = newStyle.filter;


  if (JSON.stringify(parallaxStyle) !== JSON.stringify(newStyle)) {
    newState.parallaxStyle = newStyle;
  }

  const newPlxStateClasses = getClasses(lastSegmentScrolledBy, isInSegment, parallaxData);

  if (newPlxStateClasses !== plxStateClasses) {
    newState.plxStateClasses = newPlxStateClasses;
  }

  if (Object.keys(newState).length) {
    return newState;
  }

  return null;
}

export default class Parallax extends Component {
  constructor(props) {
    super(props);

    this.handleScrollChange = this.handleScrollChange.bind(this);
    this.handleResize = this.handleResize.bind(this);

    this.state = {
      element: null,
      showElement: false,
      plxStateClasses: '',
      parallaxStyle: {},
    };
  }

  componentDidMount() {
    this.scrollManager = new ScrollManager();
    window.addEventListener('window-scroll', this.handleScrollChange);
    window.addEventListener('resize', this.handleResize);

    this.update();
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      this.update();
    }
  }

  componentWillUnmount() {
    const {
      scrollManager,
    } = this.state;

    window.removeEventListener('window-scroll', this.handleScrollChange);
    window.removeEventListener('resize', this.handleResize);

    clearTimeout(this.resizeDebounceTimeoutID);
    this.resizeDebounceTimeoutID = null;

    if (scrollManager) {
      scrollManager.removeListener();
    }
  }

  update(scrollPosition = null) {
    const currentScrollPosition = scrollPosition === null ?
      this.scrollManager.getScrollPosition().scrollPositionY : scrollPosition;

    const newState = getNewState(
      currentScrollPosition,
      this.props,
      this.state,
      this.element
    );

    if (newState) {
      requestAnimationFrame(() => this.setState(newState));
    }
  }

  handleResize() {
    clearTimeout(this.resizeDebounceTimeoutID);
    this.resizeDebounceTimeoutID = setTimeout(() => {
      this.update();
    }, RESIZE_DEBOUNCE_TIMEOUT);
  }

  handleScrollChange(e) {
    this.update(e.detail.scrollPositionY);
  }

  render() {
    const {
      children,
      className,
      disabled,
      style,
      tagName,
    } = this.props;
    const {
      showElement,
      parallaxStyle,
      plxStateClasses,
    } = this.state;

    const Tag = tagName;

    let elementStyle = style;

    if (!disabled) {
      elementStyle = {
        ...style,
        ...parallaxStyle,
        visibility: showElement ? null : 'hidden',
      };
    }

    return (
      <Tag
        {...omit(this.props, PROPS_TO_OMIT)}
        className={`Parallax ${plxStateClasses} ${className}`}
        style={elementStyle}
        ref={el => this.element = el}
      >
        {children}
      </Tag>
    );
  }
}

const propertiesItemType = PropTypes.shape({
  startValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  endValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  property: PropTypes.string.isRequired,
  unit: PropTypes.string,
});

const SafeHTMLElement = WINDOW_EXISTS ? window.HTMLElement : {};

const parallaxDataType = PropTypes.shape({
  start: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.instanceOf(SafeHTMLElement),
  ]).isRequired,
  startOffset: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  duration: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.instanceOf(SafeHTMLElement),
  ]),
  end: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.instanceOf(SafeHTMLElement),
  ]),
  endOffset: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  properties: PropTypes.arrayOf(propertiesItemType).isRequired,
  easing: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array,
    PropTypes.func,
  ]),
  name: PropTypes.string,
});


Parallax.propTypes = {
  animateWhenNotInViewport: PropTypes.bool,
  children: PropTypes.any,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  freeze: PropTypes.bool,
  parallaxData: PropTypes.arrayOf(parallaxDataType),
  style: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object])),
  tagName: PropTypes.string,
};

Parallax.defaultProps = {
  animateWhenNotInViewport: false,
  children: null,
  className: '',
  disabled: false,
  freeze: false,
  parallaxData: [],
  style: {},
  tagName: 'div',
};
