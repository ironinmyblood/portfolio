import ScrollManager from 'window-scroll-manager';
import ParallaxTransition from './Parallax';

export default ParallaxTransition;

export {
  ScrollManager,
};