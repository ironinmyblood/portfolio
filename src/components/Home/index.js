import React from 'react';
import TechstackExplosion from '../TechstackExplosion';
import Portfolio from '../Portfolio';
import Contact from '../Contact';
import Parallax from '../Parallax';
import author from '../../../public/images/author.jpg';
import {
  fadeTopAuthor,
  fadeTopTechstackHeadingInvert,
  fadeTopPotInvert,
  fadeTopPortfolioHeadingInvert
} from  './ParallaxDef'

class Home extends React.Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 intro-board">
            <Parallax
              tagName='div'
              className="w-100"
              parallaxData={fadeTopAuthor}>
              <div className="row">
                <div className="d-table w-100">
                  <div className="d-table-cell align-middle">
                    <div className="col-12 pt-5">
                      <div className="m-auto text-center pt-5 author-pic">
                        <img src={author} className="img-fluid rounded-circle" />
                      </div>

                      <div
                        tagName='h2'
                        className="author-name text-center mt-5 mb-3">
                        {" Ganapathy Raikar "}
                      </div>
                      <div
                        className="w-75 mr-auto ml-auto mt-5 mb-5">
                        <p className="author-story text-justify">{" I’m a WEB SOFTWARE PRACTIONER since 5 years, from India living in Bangalore, \
                      where I work as a UI Consultant with one of the renowned Digital Studios. \
                      Have a look at my portfolio as ive mentioned some of my project experiences. "}</p>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </Parallax>
          </div>

          <div className="col-12 pt-4 pb-4">
            <Parallax
              tagName='h3'
              className="techstackHeading text-center mb-5 pb-2"
              parallaxData={fadeTopTechstackHeadingInvert}>
              {"Tech stack hands on so far"}
            </Parallax>
            <TechstackExplosion />
            <div className="w-100 text-center">
              <Parallax
                tagName='div'
                className=""
                parallaxData={fadeTopPotInvert}>
                <i className="fas fa-mortar-pestle potImage"></i>
              </Parallax>
            </div>
          </div>

          <div className="col-12 pt-4">
            <div className='Content position-relative pr-0 pl-0 m-auto'>
              <Parallax
                tagName='h3'
                className="portfoliostackHeading opacity-0 text-center pt-5 mb-5 pb-2"
                parallaxData={fadeTopPortfolioHeadingInvert}>
                {" Portfolio "}
              </Parallax>
              <div className="w-100 clearfix tech-used">
                <h2 className="pt-4 portfolio-brief text-center"> {"These are some of the projects that I have contributed as core / supportive developer."} </h2>
              </div>
              <Portfolio />
            </div>
            <Contact />
          </div>
        </div>
      </div>
    );
  }
};

export default Home;