const fadeTopAuthor = [
    {
      start: 'self',
      startOffset: 900,
      duration: 200,
      properties: [
        {
          startValue: 1,
          endValue: 0,
          property: 'opacity',
        },
      ],
    },
  ];
  
  const fadeTopTechstackHeadingInvert = [
    {
      start: 'self',
      startOffset: 750,
      duration: 200,
      properties: [
        {
          startValue: 0,
          endValue: 1,
          property: 'opacity',
        },
        {
          startValue: 1,
          endValue: 0,
          property: 'opacity',
        }
      ],
    }
  ];
  
  const fadeTopPotInvert = [
    {
      start: 'self',
      startOffset: '40vh',
      duration: 500,
      properties: [
        {
          startValue: 0,
          endValue: 1,
          property: 'opacity',
        },
        {
          startValue: 1,
          endValue: 2,
          property: 'scale',
        },
        {
          startValue: 1,
          endValue: 0,
          property: 'opacity',
        },
      ],
    }
  ];
  
  const fadeTopPortfolioHeadingInvert = [
    {
      start: 'self',
      startOffset: 10,
      duration: 200,
      properties: [
        {
          startValue: 0,
          endValue: 1,
          property: 'opacity'
        }
      ]
    }
  ];

  module.exports = {
    fadeTopAuthor: fadeTopAuthor,
    fadeTopTechstackHeadingInvert: fadeTopTechstackHeadingInvert,
    fadeTopPotInvert: fadeTopPotInvert,
    fadeTopPortfolioHeadingInvert: fadeTopPortfolioHeadingInvert
  }