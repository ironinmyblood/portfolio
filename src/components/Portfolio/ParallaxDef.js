const fadeTopPortfolioHeadingInvert = [
    {
        start: 'self',
        startOffset: 10,
        duration: 200,
        properties: [
            {
                startValue: 0,
                endValue: 1,
                property: 'opacity'
            }
        ]
    }
];

const desktopPlx = [
    {
        start: 'self',
        startOffset: 10,
        duration: 500,
        easing: [0.25, 0.1, 0.6, 1.5],
        properties: [
            {
                startValue: 0,
                endValue: 1,
                property: 'scale',
            }
        ],
    },
];

const mobilePlx = [
    {
        start: 'self',
        startOffset: 10,
        duration: 500,
        easing: [0.25, 0.1, 0.6, 1.5],
        properties: [
            {
                startValue: 0,
                endValue: 0.9,
                property: 'scale',
            }
        ],
    },
];

const desktopGj = [
    {
        start: '.Desktop',
        startOffset: 600,
        duration: 150,
        easing: [0.39, 0.575, 0.565, 1],
        properties: [
            {
                startValue: 0,
                endValue: -59,
                unit: 'rem',
                property: 'translateY',
            }
        ],
    }
];

const mobileGj = [
    {
        start: '.Phone',
        startOffset: 600,
        duration: 200,
        easing: [0.39, 0.575, 0.565, 1],
        properties: [
            {
                startValue: 0,
                endValue: -180,
                unit: 'rem',
                property: 'translateY',
            }
        ],
    },
];

const icici = [
    {
        start: '.Desktop',
        startOffset: 1700,
        duration: 100,
        easing: [0.39, 0.575, 0.565, 1],
        properties: [
            {
                startValue: 0,
                endValue: -81,
                unit: 'rem',
                property: 'translateY',
            }
        ],
    },
];

const desktopRobosoft = [
    {
        start: '.Desktop',
        startOffset: 2600,
        duration: 100,
        easing: [0.39, 0.575, 0.565, 1],
        properties: [
            {
                startValue: 0,
                endValue: -132,
                unit: 'rem',
                property: 'translateY',
            }
        ],
    },
];

const mobileRobosoft = [
    {
        start: '.Phone',
        startOffset: 2600,
        duration: 100,
        easing: [0.39, 0.575, 0.565, 1],
        properties: [
            {
                startValue: 0,
                endValue: -89,
                unit: 'rem',
                property: 'translateY',
            }
        ],
    },
];

const bridges = [
    {
        start: '.Desktop',
        startOffset: 3600,
        duration: 100,
        easing: [0.39, 0.575, 0.565, 1],
        properties: [
            {
                startValue: 0,
                endValue: -46,
                unit: 'rem',
                property: 'translateY',
            }
        ],
    },
];

module.exports = {
    fadeTopPortfolioHeadingInvert: fadeTopPortfolioHeadingInvert,
    desktopPlx: desktopPlx,
    mobilePlx: mobilePlx,
    desktopGj: desktopGj,
    mobileGj: mobileGj,
    icici: icici,
    desktopRobosoft: desktopRobosoft,
    mobileRobosoft: mobileRobosoft,
    bridges: bridges
}