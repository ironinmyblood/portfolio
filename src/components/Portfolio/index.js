import React from 'react';
import Parallax from '../Parallax';
import {
  fadeTopPortfolioHeadingInvert,
  desktopPlx,
  mobilePlx,
  desktopGj,
  mobileGj,
  icici,
  desktopRobosoft,
  mobileRobosoft,
  bridges
} from './ParallaxDef';


class Portfolio extends React.Component {
  render() {
    return (
      <div className="portfolio-frames position-relative">
        <div className="w-100 portfolio-sections">
          <Parallax
            tagName='h3'
            className="indiviualPortfolioHeading pt-5 pb-2 opacity-0"
            parallaxData={fadeTopPortfolioHeadingInvert}>
            {" Gharajavan "}
          </Parallax>
          <div className="w-100 clearfix">
            <Parallax
              className='Desktop desktop'
              parallaxData={desktopPlx}>
              <div className='Desktop-content'>
                <Parallax
                  className='desktopContent gjDesktop'
                  parallaxData={desktopGj}
                >
                </Parallax>
              </div>
            </Parallax>
            <Parallax
              className='Phone mobile'
              parallaxData={mobilePlx}>
              <div className='Phone-content'>
                <Parallax
                  className='phoneContent gjMobile'
                  parallaxData={mobileGj}
                >
                </Parallax>
              </div>
            </Parallax>
          </div>
          <div className="w-100 clearfix tech-used">
            <h2 className="mb-3"> {"Technologies used : "} </h2>
            <span className="badge badge-pill badge-warning"> {" HTML5 "} </span>
            <span className="badge badge-pill badge-warning"> {" BOOTSTRAP4 "} </span>
            <span className="badge badge-pill badge-warning"> {" ES5 "} </span>
            <span className="badge badge-pill badge-warning"> {" ajax "} </span>
            <span className="badge badge-pill badge-warning"> {" jQuery "} </span>
            <span className="badge badge-pill badge-warning"> {" CODEIGNITOR "} </span>
            <span className="badge badge-pill badge-warning"> {" MYSQL "} </span>
          </div>

          <div className="w-100 clearfix tech-used">
            <h2 className="mb-3"> {"About project : "} </h2>
            <p>
              {" Gharajavan is a food blog, that serve people with info on simple home made recipes Eg breakfast, dinner, local cuisines, spicy non vegan dishes etc. The blog consists of features\
                 like tag / full text search search, user login with email notified for related operations, genuine user comment & reply with email notifications"}
            </p>
          </div>
        </div>

        <div className="w-100 portfolio-sections">

          <Parallax
            tagName='h3'
            className="indiviualPortfolioHeading pt-5 pb-2 opacity-0"
            parallaxData={fadeTopPortfolioHeadingInvert}>
            {" ICIC Kiosk "}
          </Parallax>
          <div className="w-100 clearfix">
            <Parallax
              className='Desktop desktop offset-2'
              parallaxData={desktopPlx}>
              <div className='Desktop-content'>
                <Parallax
                  className='desktopContent'
                  parallaxData={icici}
                >
                  <div className="desktopContent ikDesktop1"></div>
                  <div className="desktopContent ikDesktop2"></div>
                  <div className="desktopContent ikDesktop3"></div>
                </Parallax>
              </div>
            </Parallax>
          </div>
          <div className="w-100 clearfix tech-used">
            <h2 className="mb-3"> {"Technologies used : "} </h2>
            <span className="badge badge-pill badge-warning"> {" HTML5 "} </span>
            <span className="badge badge-pill badge-warning"> {" BOOTSTRAP3 "} </span>
            <span className="badge badge-pill badge-warning"> {" SASS "} </span>
            <span className="badge badge-pill badge-warning"> {" ES5 "} </span>
            <span className="badge badge-pill badge-warning"> {" HAMMER Js "} </span>
            <span className="badge badge-pill badge-warning"> {" GULP "} </span>
            <span className="badge badge-pill badge-warning"> {" YEOMAN "} </span>
          </div>

          <div className="w-100 clearfix tech-used">
            <h2 className="mb-3"> {"About project : "} </h2>
            <p>
              {" ICICI kiosk was buit to provide a rich UI experience for ICICI customers while operating kiosk machines. \
              The development involved about 34 screens for different operations with light weight plugins utilized \
              in order to support the performance point of view being a banking application. I wrote the lightweight javascript plugins such as onscreen keyboard, user touch panel in main screen thst gave pallete view for nested menu and a dynamic menu drawer thats capable to handle viewport size / menu quanity."}
            </p>
          </div>
        </div>

        <div className="w-100 portfolio-sections">
          <Parallax
            tagName='h3'
            className="indiviualPortfolioHeading pt-5 pb-2 opacity-0"
            parallaxData={fadeTopPortfolioHeadingInvert}>
            {" Robosoft "}
          </Parallax>
          <div className="w-100 clearfix">
            <Parallax
              className='Desktop desktop'
              parallaxData={desktopPlx}>
              <div className='Desktop-content'>
                <Parallax
                  className='desktopContent rsDesktop'
                  parallaxData={desktopRobosoft}
                >
                </Parallax>
              </div>
            </Parallax>
            <Parallax
              className='Phone mobile'
              parallaxData={mobilePlx}>
              <div className='Phone-content'>
                <Parallax
                  className='phoneContent rsMobile'
                  parallaxData={mobileRobosoft}
                >
                </Parallax>
              </div>
            </Parallax>
          </div>
          <div className="w-100 clearfix tech-used">
            <h2 className="mb-3"> {"Technologies used : "} </h2>
            <span className="badge badge-pill badge-warning"> {" HTML5 "} </span>
            <span className="badge badge-pill badge-warning"> {" BOOTSTRAP3 "} </span>
            <span className="badge badge-pill badge-warning"> {" SASS "} </span>
            <span className="badge badge-pill badge-warning"> {" ES5 "} </span>
            <span className="badge badge-pill badge-warning"> {" GRUNT "} </span>
            <span className="badge badge-pill badge-warning"> {" YEOMAN "} </span>
          </div>

          <div className="w-100 clearfix tech-used">
            <h2 className="mb-3"> {"About project : "} </h2>
            <p>
              {" The UI built for a private company, Robosoft Technologies. \
              Since Robosoft followed the DNA of Apple Inc as it was one of the first companies who served Apple while it \
              first introduced MAC to India. The website has accordions, nested menu & pages, carousels, animated transition displays \
              for HD images of mobile apps etc. Also the onscroll parallax to provide some of thr fancy interactions across website."}
            </p>
          </div>
        </div>

        <div className="w-100 portfolio-sections">

          <Parallax
            tagName='h3'
            className="indiviualPortfolioHeading pt-5 pb-2 opacity-0"
            parallaxData={fadeTopPortfolioHeadingInvert}>
            {" Bridges Mordernization "}
          </Parallax>
          <div className="w-100 clearfix">
            <Parallax
              className='Desktop desktop offset-2'
              parallaxData={desktopPlx}>
              <div className='Desktop-content'>
                <Parallax
                  className='desktopContent'
                  parallaxData={bridges}
                >
                  <div className="desktopContent bridgesDesktop1"></div>
                  <div className="desktopContent bridgesDesktop2"></div>
                </Parallax>
              </div>
            </Parallax>
          </div>

          <div className="w-100 clearfix tech-used">
            <h2 className="mb-3"> {"Technologies used : "} </h2>
            <span className="badge badge-pill badge-warning"> {" MATERIAL UI "} </span>
            <span className="badge badge-pill badge-warning"> {" JSS "} </span>
            <span className="badge badge-pill badge-warning"> {" REACT Js "} </span>
            <span className="badge badge-pill badge-warning"> {" ES6 "} </span>
            <span className="badge badge-pill badge-warning"> {" WEBPACK 3 "} </span>
            <span className="badge badge-pill badge-warning"> {" ESLINT "} </span>
            <span className="badge badge-pill badge-warning"> {" BABEL "} </span>
            <span className="badge badge-pill badge-warning"> {" JEST "} </span>
            <span className="badge badge-pill badge-warning"> {" REDUX "} </span>
            <span className="badge badge-pill badge-warning"> {" NODE Js "} </span>
          </div>

          <div className="w-100 clearfix tech-used">
            <h2 className="mb-3"> {"About project : "} </h2>
            <p>
              {" It is the portal accessible by citizens, monitored by their Federal Govt. The portal has feature rich UI having many reuasable components\
               built to reduce complexity across verious forms involving complex operations."}
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default Portfolio;