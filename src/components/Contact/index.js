import React from 'react';
import animateScroll from 'animated-scroll-to';

class Contact extends React.Component {
    handleScrollTop() {
        animateScroll(0, { minDuration: 3000 });
    }
    render() {
        return (
            <div className='row Footer'>
                <div className='col-10 col-sm-10 col-md-4 col-lg-4 position-relative pr-0 pl-0 m-auto'>
                    <h1 className="footerHeading">{"Contact"}</h1>
                    <p className="email mt-5 mb-3">
                        <span className="label d-inline">{"work: "}</span>
                        <span className="email-id d-inline">
                            <a href="mailto:rgraiker@deloitte.com">{"graiker@deloitte.com"}</a>
                        </span>
                    </p>
                    <p className="email mt-4 mb-3">
                        <span className="label d-inline">{"private: "}</span>
                        <span className="email-id d-inline">
                            <a href="mailto:raiker123@gmail.com">{"raiker123@gmail.com"}</a>
                        </span>
                    </p>
                    <p className="number">
                        <span className="icon d-inline">
                            <i className="fab fa-telegram-plane"></i>
                        </span>
                        <span className="tag-names d-inline">
                            &nbsp;&nbsp;{"@gannu_raikar"}
                        </span>
                    </p>
                    <p className="number">
                        <span className="icon d-inline">
                            <i className="fab fa-whatsapp"></i>
                        </span>
                        <span className="tag-names d-inline">
                            <a href="tel:+91-953-865-3909">&nbsp;&nbsp;{"+91-953-865-3909"}</a>
                        </span>
                    </p>
                    <button onClick={() => this.handleScrollTop()}>Back to top</button>
                </div>
            </div>
        );
    }
}

export default Contact;