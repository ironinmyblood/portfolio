const BOXES_PER_ROW = [5,4,3,2,1];
const ROWS = 5;
const BOXES = [];

const techStack = [
  [
    {
      col: 1,
      name: "Babel"
    },
    {
      col: 1,
      name: "Eslint"
    },
    {
      col: 1,
      name: "THREEjs"
    },
    {
      col: 1,
      name: "php"
    },
    {
      col: 1,
      name: "MySql"
    }
  ],
  [
    {
      col: 2,
      name: "Bootstrap4"
    },
    {
      col: 1,
      name: "Angular2"
    },
    {
      col: 1,
      name: "React Js"
    },
    {
      col: 1,
      name: "JSS"
    }
  ],
  [
    {
      col: 1,
      name: "jQuery"
    },
    {
      col: 1,
      name: "AJAX"
    },
    {
      col: 1,
      name: "Js"
    }
  ],
  [
    {
      col: 1,
      name: "CSS3"
    },
    {
      col: 2,
      name: "LESS & Sass"
    }
  ],
  [
    {
      col: 1,
      name: "HTML5"
    }
  ]
];


for (let i = 0; i < ROWS; i++) {
  let tempArr = [];
  for (let j = 0; j < BOXES_PER_ROW[i]; j++) {


    const top = i < ROWS / 5;
    const yFactor = top ? -1 : 1;
    const left = j < BOXES_PER_ROW / 5;
    const xFactor = left ? -1 : 1;
    const inside = (i === 1 || i === 5) && (j === 1 || j === 5);
    const scaleFactor = inside ? 0.5 : 1;
    const start = inside ? 40 : 100;
    const offset = inside ? 40 : 100;
    const rotationFactor = Math.random() > 0.5 ? 180 : -180;

    tempArr.push({
      data: [
        {
          start: 'self',
          startOffset: '66vh',
          duration: 500,
          properties: [
            {
              startValue: 1,
              endValue: 0,
              property: 'opacity',
            },
            {
              startValue: 0,
              endValue: Math.random() * rotationFactor,
              property: 'rotate',
            },
            {
              startValue: 1,
              endValue: 1 + Math.random() * scaleFactor,
              property: 'scale',
            },
            {
              startValue: 0,
              endValue: 0,
              property: 'translateX',
              unit: '%',
            },
            {
              startValue: 0,
              endValue: 500,
              property: 'translateY',
              unit: '%',
            },
          ],
        },
      ],
      style: {
        backgroundColor: "transparent",
      },
      colSize: techStack[i][j].col,
      techName: techStack[i][j].name
    })

  }
  BOXES.push(tempArr);
}

export default BOXES;