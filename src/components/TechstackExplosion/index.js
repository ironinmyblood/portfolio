import React from 'react';
import Parallax from '../Parallax'
import TechNames from './ParallaxDef'

class TechstackExplosion extends React.Component {
  render() {
    return (
      <div className='w-100'>
        {
          TechNames.map((row, index) => (
            <div
              key={index}
              className="row justify-content-center">
              {row.map((box, boxIndex) => (
                <Parallax
                  key={`${index} ${boxIndex}`}
                  className={`col-${box.colSize} explosive-box text-center`}
                  parallaxData={box.data}
                  style={box.style}
                >
                  {box.techName}
                </Parallax>
              ))}
            </div>
          ))
        }
      </div>
    );
  }
}

export default TechstackExplosion;