
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './components/Home';
import Loader from 'react-loader-advanced';
import './styles/docs.scss';

// LOADER
const LoaderView = () => (
  <div className="loading">
    <div className="finger finger-1">
      <div className="finger-item">
        <span></span><i></i>
      </div>
    </div>
    <div className="finger finger-2">
      <div className="finger-item">
        <span></span><i></i>
      </div>
    </div>
    <div className="finger finger-3">
      <div className="finger-item">
        <span></span><i></i>
      </div>
    </div>
    <div className="finger finger-4">
      <div className="finger-item">
        <span></span><i></i>
      </div>
    </div>
    <div className="last-finger">
      <div className="last-finger-item"><i></i></div>
    </div>
  </div>
);

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        loader: false
      })
      document.body.classList.remove('modal-open')
    },
    1800);
  }
  render() {
    const { loader } = this.state;

    return (
      <Loader
        show={loader}
        message={<LoaderView />}>
        <Router>
          <Switch>
            <Route exact path={'/home'} component={Home} />
            <Route component={Home} />
          </Switch>
        </Router>
      </Loader>
    );
  }
}

export default App;


