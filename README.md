# portfolio - ganapathy raikar
 
A simple project setup with reactjs, webpack, babel and customised react-parallax plugin to showcase few aspects of portfolio.


## Install and Running
`git clone https://ironinmyblood@bitbucket.org/ironinmyblood/portfolio.git`

 1. npm install
 2. npm start
 3. navigate to http://localhost:8080 in your browser of choice.
 4. npm run build for minified build and one can check by http-server at their local.
