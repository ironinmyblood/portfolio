const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const NODE_ENV = process.env.NODE_ENV || 'development';
const isProduction = NODE_ENV === 'production';
const distPath = path.join(__dirname, './dist');
const sourcePath = path.join(__dirname, './src');

const plugins = [];
const entry = {};

// COMMON RULES
const rules = [
  {
    test: /\.(js|jsx)$/,
    exclude: /node_modules/,
    use: [
      'babel-loader'
    ]
  },
  {
    test: /\.(png|gif|jpg|svg)$/,
    use: 'file-loader?name=[name]-[hash].[ext]'
  }
];

if (isProduction) {
  // PROD ENTRY POINTS
  entry['portfolio.min'] = path.join(sourcePath, 'index.js');

  // PROD PLUGINS
  plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      include: /\.min\.js$/,
      compress: {
        warnings: false,
        screw_ie8: true,
        conditionals: true,
        unused: true,
        comparisons: true,
        sequences: true,
        dead_code: true,
        evaluate: true,
        if_return: true,
        join_vars: true,
      },
      output: {
        comments: false
      }
    }),
    new ExtractTextPlugin('portfolio.min.css'),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false
    })
  );

    plugins.push(
      new HtmlWebpackPlugin({
        template: path.join(sourcePath, 'index.html'),
        path: distPath,
        filename: 'index.html',
        favicon: path.join(__dirname, 'public/raikar.ico')
      })
    );

  // PROD RULES
  rules.push(
    {
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: 'css-loader!sass-loader'
      })
    }
  );
} else {
  // DEV ENTRY POINTS
  entry.main = path.join(sourcePath, 'index.js');
  // DEV PLUGINS
  plugins.push(
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: path.join(sourcePath, 'index.html'),
      path: distPath,
      filename: 'index.html',
      favicon: path.join(__dirname, 'public/raikar.ico')
    })
  );

  // DEV RULES
  rules.push(
    {
      test: /\.(scss|css)$/,
      use: [
        'style-loader',
        'css-loader',
        'sass-loader',
      ]
    }
);
}

module.exports = {
  devtool: isProduction ? false : 'source-map',
  entry,
  output: {
    path: distPath,
    filename: '[name].js',
  },
  module: {
    rules,
  },
  resolve: {
    extensions: ['.webpack-loader.js', '.web-loader.js', '.loader.js', '.js', '.jsx'],
    modules: [
      path.resolve(__dirname, 'node_modules'),
    ]
  },
  plugins,
  devServer: {
    historyApiFallback: true,
    compress: isProduction,
    inline: !isProduction,
    hot: !isProduction,
    disableHostCheck: true,
    stats: {
      assets: true,
      children: false,
      chunks: false,
      hash: false,
      modules: false,
      publicPath: false,
      timings: true,
      version: false,
      warnings: true
    },
  },
};